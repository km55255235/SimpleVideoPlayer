package com.example.simplevideoplayer

import android.media.MediaPlayer
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.MediaController
import com.example.simplevideoplayer.databinding.ActivityMainBinding
import com.google.android.exoplayer2.MediaItem

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val videoURL =
        "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"
    private val videoUri = Uri.parse(videoURL)

    private lateinit var mediaPlayer: MediaPlayer
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //untuk load video melalui video view.
        binding.videoView.setVideoURI(videoUri)
        val mediaController = MediaController(this)
        mediaController.setMediaPlayer(binding.videoView)

        //Video View di set Media controllernya agar ada controller play, forward, backward
        binding.videoView.setMediaController(mediaController);
        binding.videoView.requestFocus()
        binding.videoView.start()

        //klo mau custom actionnya bisa aja
        binding.videoView.setOnPreparedListener {
            mediaPlayer = it
        }
        binding.playButton.setOnClickListener {
            resume()
        }
        binding.stopButton.setOnClickListener {
            pause()
        }
    }

    fun pause() {
        mediaPlayer.pause()
    }

    fun resume(){
        mediaPlayer.start()
    }
}